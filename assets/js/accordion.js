$(document).on('click', '.js-accordion_link', function(event) {
	event.preventDefault();

	$('.js-accordion_link').removeClass('active');
	$('.js-accordion_content').slideUp('400');

	$(this).addClass('active').parent().find('.js-accordion_content').slideDown('400');
});
